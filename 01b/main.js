function loadDoc() {
  
  var url = 'http://192.168.100.76:5984/_all_dbs'	
  // Petición: instanciar nuevo XMLHttpRequest, cargo en el objeto (var xhttp) la clase (new...)
  var xhttp = new XMLHttpRequest();
 
  // definir callback para onreadystatechange que es una propiedad de objeto xhttp que dentro tiene una funcion que procesa la respuesta
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {    /* readyState es una propiedad que puede tener los siguientes valores: 0 (no inicializada),1 (leyendo)
                                                         2 (leido), 3 (interactiva),4 (completo)  y status : el estado de la comunicación*/
  
   //   console.log(this.responseText) enseña por consola la respuesta una vez que la condicion del if se cumple
    
   	console.log( JSON.parse(this.responseText) ) // otra manera de presentar los valores por consola
    }

  };
 
  // establecer, método, url y asincronicidad..., el método open tiene 3 variables : método, url y asincronicidad
  xhttp.open("GET", url, true);
 
  // enviar la petición
  xhttp.send();
}
