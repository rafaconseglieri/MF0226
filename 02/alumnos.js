const database = 'http://localhost:5984/garbage/'

var defaultAjax = {
	  type: "get",
	  url: database + "_all_docs?include_docs=true",	  	  
	  dataType: 'json',	  
	  contentType: "application/json; charset=utf-8"
}

function getAlumnos(){
  defaultAjax.type = "get",
  defaultAjax.url = database + "_all_docs?include_docs=true",	  	  
  $.ajax(defaultAjax).done( function(data) {   	
  	$('#selector').html('')
  	$.each(data.rows,function(idx,row){
  		console.log(row.doc._id, row.doc.apellidos, row.doc.nombre);
  		let html = `
  		<option value="${row.id}">
  		${row.doc.apellidos}, ${row.doc.nombre}
  		</option>
  		`
  		$('#selector').append(html)
  	});
  });  
}	

function cambiaCombo(){	
	defaultAjax.type ='get'
	defaultAjax.url = database + $('#selector option:selected').val()	
	$.ajax(defaultAjax).done(function(data){
	  $('#id').val(data._id)
	  $('#rev').val(data._rev)
	  $('#apellidos').val(data.apellidos)
	  $('#nombre').val(data.nombre)
	})
	
}

function modificar(){
  // 1.- recoger los inputs del formulario en un objeto
  let doc = {
  	_id: $('#id').val(),
  	_rev: $('#rev').val(),
  	apellidos: $('#apellidos').val(),
  	nombre: $('#nombre').val()
  }
  // 2.- enviar POST a COuchdb
  defaultAjax.type = 'post'
  defaultAjax.url = database 
  defaultAjax.data = JSON.stringify(doc)
  $.ajax(defaultAjax).done( function(data){ 	
  })
  getAlumnos()
  
}

$(document).ready( function(){
  getAlumnos()
});
